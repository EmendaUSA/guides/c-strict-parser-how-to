# C++ Strict Parser - How to get an accurate project

This is the supporting project on how to set up a C/C++ project in SciTools Understand to MAXIMIZE accuracy.

## Check out the YouTube video!

The video can be seens on [Stephane Software Web Channel](https://www.youtube.com/watch?v=PFSWDGITZr0).

## Get the supporting .PDF for reference

You can download the presentation [here](./CStrictAnalysisGuide.pdf).
